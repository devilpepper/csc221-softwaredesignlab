import java.math.BigInteger;

// Fibonacci generates Fibonacci series.
// A Fibonacci series F is a sequence of integers where F(1) = F(2) = 1 and, for i > 2, F(i) = F(i-1) + F(i-2)
public class Fibonacci {

	final int NUMS = 100;	// NUM is the number of Fibonacci numbers to generate

	public static void main(String[] args) {

		Fibonacci fib = new Fibonacci();

		// Print the first NUMS Fibonacci numbers (Method 1)
		fib.doFibonacci();
		// Print the first NUMS Fibonacci numbers (Method 2)
//		fib.doFibonacciBig();
		
//		fib.doFibonacciRecursivelyReverse(10, 1, 0);
//		fib.doFibonacciRecursively(1, 1, 0);
	}

	// doFibonacci processes a Fibonacci sequence
	private void doFibonacci() {
		
		long prev = 1;	// prev is F(i-2) (2 < i <= NUMS)
		long next = 1;	// next is F(i-1) (2 < i <= NUMS)
		long curr;		// curr is F(i) (2 < I <= NUMS)
		
		// If we need to process one or more Fibonacci numbers ...
		if (NUMS >= 1) {
			// ...print the first Fibonacci number, F(1) ...
			System.out.println("1: " + prev);
			// ...and if we need to process two or more Fibonacci numbers...
			if (NUMS >= 2) {
				// ...print the second Fibonacci number, F(2) ...
				System.out.println("2: " + next);				
				// ...and for all subsequent Fibonacci numbers F(i), 2 < i <= NUMS,...
				for (int i = 3; i <= NUMS; i++) {
					// ...compute F(i-2) + F(i-1)...
					curr = prev + next;
					// ...and print F(i).
					System.out.printf("%d: %d%n", i, curr);
					prev = next;	// Update F(i-2)
					next = curr;	// Update F(i-1)
				}
			}
		}

	}

	// doFibonacciVanilla processes a large Fibonacci sequence
	private void doFibonacciBig() {

		BigInteger prev = new BigInteger("1");	// prev is F(i-2) (2 < i <= NUMS)
		BigInteger next = new BigInteger("1");	// next is F(i-1) (2 < i <= NUMS)
		
		// If we need to process one or more Fibonacci numbers ...
		if (NUMS >= 1) {
			// ...print the first Fibonacci number, F(1) ...
			System.out.println("1: " + prev);
			// ...and if we need to process two or more Fibonacci numbers...
			if (NUMS >= 2) {
				// ...print the second Fibonacci number, F(2) ...
				System.out.println("2: " + next);
				// ...and for all subsequent Fibonacci numbers F(i), 2 < i <= NUMS,...
				for (int i = 3; i <= NUMS; i++) {
					// ...compute F(i-2) + F(i-1)...
					BigInteger curr = next;
					curr = curr.add(prev);
					// ...and print F(i).
					System.out.printf("%d: %d%n", i, curr);
					prev = next;	// Update F(i-2)
					next = curr;	// Update F(i-1)
				}
			}
		}

	}

	private void doFibonacciRecursivelyReverse(int n, int Fn, int Fprev) {
		if (n != 0) {
			doFibonacciRecursivelyReverse(n-1, Fn + Fprev, Fn);
			System.out.printf("%d: %d%n", n, Fn);
		}
	}

	private void doFibonacciRecursively(int n, int Fn, int Fprev) {
		
		System.out.printf("%d: %d%n", n, Fn);		
		if (n < NUMS) {
			doFibonacciRecursively(n+1, Fn + Fprev, Fn);
		}
	}

}
