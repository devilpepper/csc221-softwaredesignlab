import java.io.*;
import java.math.BigInteger;

// Fibonacci generates Fibonacci series.
// A Fibonacci series F is a sequence of integers where F(1) = F(2) = 1 and, for i > 2, F(i) = F(i-1) + F(i-2)
public class Fibonacci2 {

	final int NUMS = 100;	// NUM is the number of Fibonacci numbers to generate

	public static void main(String[] args) {

		Fibonacci2 fib = new Fibonacci2();
		// Print the first NUMS Fibonacci numbers (Method 1)
//		fib.doFibonacci();
		// Print the first NUMS Fibonacci numbers (Method 2)
//		fib.doFibonacciBig();
		// Print the first NUMS Fibonacci numbers in reverse (Method 3, using recursion)
//		fib.doFibonacciRecursivelyReverse(10, 1, 0);
		// Print the first NUMS Fibonacci numbers (Method 4, using recursion)
//		fib.doFibonacciRecursively(1, 1, 0);

		fib.writeFibSeqv1();
//		fib.writeFibSeqv2();

	}

	private void writeFibSeqv1() {

 		try {
 			String fileName = "fib2.txt";
 			FileOutputStream outStream = new FileOutputStream(fileName);
 			OutputStreamWriter outStreamWriter = new OutputStreamWriter(outStream);
 			BufferedWriter bw = new BufferedWriter(outStreamWriter);
 			
// 			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
// 			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
//	 		doFibonacci(bw);	 		
	 		doFibonacciBig(bw);
 	 		bw.close();
 		} catch (IOException e) {
			System.out.println(e.toString()); 			
 		}

	}

	private void writeFibSeqv2() {

 		try {
 			BufferedWriter bw = new BufferedWriter(new FileWriter("fib2.txt"));
 	 		doFibonacci(bw);
 //	 		doFibonacciBig(bw);
			bw.close();
 		} catch (IOException e) {
			System.out.println(e.toString()); 			
 		}

	}

	// doFibonacci processes a Fibonacci sequence
	private void doFibonacci(BufferedWriter bw) {
		
		int prev = 1;	// prev is F(i-2) (2 < i <= NUMS)
		int next = 1;	// next is F(i-1) (2 < i <= NUMS)
		int curr;		// curr is F(i) (2 < I <= NUMS)
		
		// If we need to process one or more Fibonacci numbers ...
		if (NUMS >= 1) {
			// ...print the first Fibonacci number, F(1) ...
			System.out.println("1: " + prev);
			try {
				bw.write("1: " + Integer.toString(prev));
				bw.newLine();
			} catch (IOException e) {
				System.out.println(e.toString());
			}
			// ...and if we need to process two or more Fibonacci numbers...
			if (NUMS >= 2) {
				// ...print the second Fibonacci number, F(2) ...
				System.out.println("2: " + next);
				try {
					bw.write("2: " + Integer.toString(next));
					bw.newLine();
				} catch (IOException e) {
					System.out.println(e.toString());
				}
				// ...and for all subsequent Fibonacci numbers F(i), 2 < i <= NUMS,...
				for (int i = 3; i <= NUMS; i++) {
					// ...compute F(i-2) + F(i-1)...
					curr = prev + next;
					// ...and print F(i).
					System.out.printf("%d: %d%n", i, curr);
					try {
						bw.write(Integer.toString(i) + ": " + Integer.toString(curr));
						bw.newLine();
					} catch (IOException e) {
						System.out.println(e.toString());
					}
					prev = next;	// Update F(i-2)
					next = curr;	// Update F(i-1)
				}
			}
		}

	}

	// doFibonacciVanilla processes a large Fibonacci sequence
	private void doFibonacciBig(BufferedWriter bw) {

		BigInteger prev = new BigInteger("1");	// prev is F(i-2) (2 < i <= NUMS)
		BigInteger next = new BigInteger("1");	// next is F(i-1) (2 < i <= NUMS)
		
		// If we need to process one or more Fibonacci numbers ...
		if (NUMS >= 1) {
			// ...print the first Fibonacci number, F(1) ...
			System.out.println("1: " + prev);
			try {
//				bw.write("1: " + prev);
				bw.write(prev.toString());
				bw.newLine();
			} catch (IOException e) {
				System.out.println(e.toString());
			}
			// ...and if we need to process two or more Fibonacci numbers...
			if (NUMS >= 2) {
				// ...print the second Fibonacci number, F(2) ...
				System.out.println("2: " + next);
				try {
//					bw.write("2: " + next);
					bw.write(next.toString());
					bw.newLine();
				} catch (IOException e) {
					System.out.println(e.toString());
				}
				// ...and for all subsequent Fibonacci numbers F(i), 2 < i <= NUMS,...
				for (int i = 3; i <= NUMS; i++) {
					// ...compute F(i-2) + F(i-1)...
					BigInteger curr = next;
					curr = curr.add(prev);
					// ...and print F(i).
					System.out.printf("%d: %d%n", i, curr);
					try {
//						bw.write(Integer.toString(i) + ": " + curr);
						bw.write(curr.toString());
						bw.newLine();
					} catch (IOException e) {
						System.out.println(e.toString());
					}
					prev = next;	// Update F(i-2)
					next = curr;	// Update F(i-1)
				}
			}
		}

	}

	private void doFibonacciRecursivelyReverse(int n, int Fn, int Fprev) {
		if (n != 0) {
			doFibonacciRecursivelyReverse(n-1, Fn + Fprev, Fn);
			System.out.printf("%d: %d%n", n, Fn);
		}
	}

	private void doFibonacciRecursively(int n, int Fn, int Fprev) {
		
		System.out.printf("%d: %d%n", n, Fn);		
		if (n < NUMS) {
			doFibonacciRecursively(n+1, Fn + Fprev, Fn);
		}
	}

}
